# Trabajo Final de la materia Dise�o de Base de Datos 

App: BrownBagging

## Descripcion

Herramienta web para gestion de reuniones informales.

Tools:
	
	- FrontEnd: Angular 4
	- BackEnd: Java (Spring Boot)

## Instalacion

### Front-End

- Instalar [NodeJs 6.10.3](https://nodejs.org/download/release/v6.10.3/)
- Ir por consola a la carpeta raiz del proyecto (front-end):
	- `npm install -g @angular/cli@1.4.2` (Instalacion de Angular Cli)
	- `npm install`
- Para correr el proyecto: `ng serve -env=dev`
- Para ejecutar los tests:
	- Unit Test: `ng test -env=mock`
	- E2E Test: `ng e2e -env=mock`

### Back-End

- Instalar [Gradle](https://gradle.org/install/)
- MongoDB o MySQL
	- MySQL
		- Borrar el archivo `/src/develop/backend/brownbaggingAPI/src/main/java/com/lf/brownbaggingAPI/models/Meeting_MongoDB.java`
		- Dentro del archivo `/src/develop/backend/brownbaggingAPI/src/main/resources/application.properties`
			- Modificar la linea `spring.datasource.url=jdbc:mysql://SERVER_IP:PORT/DB_NAME`
			- Modificar la linea `spring.datasource.username=DB_USER`
			- Modificar la linea `spring.datasource.password=DB_USER_PASS`
	- MongoDB
		- Borrar el archivo `/src/develop/backend/brownbaggingAPI/src/main/java/com/lf/brownbaggingAPI/models/Meeting.java`
		- Renombrar el archivo `/src/develop/backend/brownbaggingAPI/src/main/java/com/lf/brownbaggingAPI/models/Meeting_MongoDB.java` a `Meeting.java`
		- Dentro del archivo `/src/develop/backend/brownbaggingAPI/src/main/resources/application.properties`
			- Eliminar la seccion **Configuracion para utilizar MySQL** y eliminar el \# de la seccion **Configuracion para utilizar MongoDB**			
			- Modificar la linea `spring.data.mongodb.uri=mongodb://SERVER_IP:PORT/DB_NAME`
- Ir por consola a la carpeta raiz del proyecto (back-end)
	- `sudo gradle build`
	- `gradle bootrun`
- Cambiar puerto (opcional)
	- Modificar el archivo `/src/develop/backend/brownbaggingAPI/src/main/resources/application.properties`
		- Cambiar el puerto en la linea `server.port = 35555` a `server.port = PORTNUMBER`		

## Colaboradores

- Labrune Juan Pablo
- Fontan Alejandro