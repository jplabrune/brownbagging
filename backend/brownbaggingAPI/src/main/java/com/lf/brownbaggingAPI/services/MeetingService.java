package com.lf.brownbaggingAPI.services;

import com.lf.brownbaggingAPI.models.Meeting;
import com.lf.brownbaggingAPI.repositories.MeetingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MeetingService {
    @Autowired
    MeetingRepository meetingRepository;

    public List<Meeting> getAllMeeting(){
        List<Meeting> meetings = new ArrayList<Meeting>();
        this.meetingRepository.findAll()
                .forEach(meetings::add);
        return meetings;
    }

    public Meeting getMeeting(Integer id){
        return this.meetingRepository.findOne(id);
    }

    public void addMeeting(Meeting meeting){
        this.meetingRepository.save(meeting);
    }

    public void updateMeeting(Integer id, Meeting meeting){
        this.meetingRepository.save(meeting);
    }

    public void deleteMeeting(Integer id){
        this.meetingRepository.delete(id);
    }

}
