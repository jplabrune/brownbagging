package com.lf.brownbaggingAPI.repositories;

import com.lf.brownbaggingAPI.models.Meeting;
import org.springframework.data.repository.CrudRepository;

public interface MeetingRepository extends CrudRepository<Meeting, Integer> {

}
