package com.lf.brownbaggingAPI.controllers;

import com.lf.brownbaggingAPI.models.CustomErrorType;
import com.lf.brownbaggingAPI.models.Meeting;
import com.lf.brownbaggingAPI.services.MeetingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MeetingController {

    @Autowired
    private MeetingService meetingService;

    @CrossOrigin()
    @GetMapping("/meetings")
    public List<Meeting> getAllMeeting() {
        return this.meetingService.getAllMeeting();
    }

    @CrossOrigin()
    @GetMapping("/meeting/{id}")
    public ResponseEntity<Meeting> getMeetingById(@PathVariable(value = "id") Integer id_meeting) {
        Meeting meeting = meetingService.getMeeting(id_meeting);
        if(meeting == null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(meeting);
    }

    @CrossOrigin()
    @PostMapping("/meeting")
    public ResponseEntity<Meeting> createMeeting(@Valid @RequestBody Meeting meeting) {
        this.meetingService.addMeeting(meeting);
        return ResponseEntity.ok(meeting);
    }

    @CrossOrigin()
    @PutMapping("/meeting/{id}")
    public ResponseEntity<Meeting> updateMeeting(@PathVariable(value = "id") Integer id_meeting,
                                                  @Valid @RequestBody Meeting meetingRecieved) {
        Meeting meeting = this.meetingService.getMeeting(id_meeting);
        if (meeting == null) {
            return new ResponseEntity(new CustomErrorType("Unable to update. Meeting with id " + id_meeting + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        if(!meeting.getLastUpdateDatetime().equals(meetingRecieved.getLastUpdateDatetime())){
            return new ResponseEntity(new CustomErrorType("Unable to update. The meeting you are trying to update has been updated before."),
                    HttpStatus.CONFLICT);
        }

        meetingRecieved.setId(id_meeting);
        meetingRecieved.setLastUpdateDatetime(new Timestamp(System.currentTimeMillis()));

        this.meetingService.updateMeeting(id_meeting, meetingRecieved);
        return ResponseEntity.ok(meetingRecieved);
    }

    @CrossOrigin()
    @DeleteMapping("/meeting/{id}")
    public ResponseEntity<Meeting> deleteAlquiler(@PathVariable(value = "id") Integer id_meeting) {
        Meeting meeting = this.meetingService.getMeeting(id_meeting);
        if(meeting == null) {
            return new ResponseEntity(new CustomErrorType("Unable to delete. Meeting with id " + id_meeting + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        this.meetingService.deleteMeeting(id_meeting);
        return ResponseEntity.ok().build();
    }

}
