package com.lf.brownbaggingAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrownbaggingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrownbaggingApiApplication.class, args);
	}
}
