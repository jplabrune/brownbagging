import { Component, OnInit } from '@angular/core';
import { ToastrService, ToastrConfig } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MdSelectModule } from '@angular/material';

import { AuthGuardService } from '../shared/auth-guard.service';
import { MeetingService } from '../meeting/meeting.service';
import { Meeting } from '../meeting/meeting';

@Component({
  selector: 'app-meeting-list',
  templateUrl: './meeting-list.component.html',
  styleUrls: ['./meeting-list.component.css'],
})
export class MeetingListComponent implements OnInit {
	meetings:  Array<Meeting> = [];
	filterValue: string;
	filterBy: string;
	searchValue: string;
	showLoading: Boolean = true;
	closeResult: string;

	selectedValue: string;
	sortBy = [
		{value: 'title', viewValue: 'Title'},
		{value: 'speaker', viewValue: 'Speaker'},
		{value: 'lastUpdate', viewValue: 'LastUpdate'},
		{value: 'initDate', viewValue: 'Init Date'}
	];

	constructor(
		private meetingService: MeetingService,
		private toastrService: ToastrService,
		private modalService: NgbModal,
		private mdSelectModule: MdSelectModule,
		private authGuardService: AuthGuardService) {}

	ngOnInit() {
		this.getMeetings();
	}

	public getMeetings() {
		this.meetingService.getMeetings().subscribe(
			meetingsResponse => {
				this.meetings = meetingsResponse;
				this.sortMeetings('title', this.meetings);
				this.showLoading = false;
			},
			error => this.toastrService.error('Error when loading meetings', error)
		);
	}

	public deleteMeeting(meetingId) {
		this.meetingService.deleteMeetingById(meetingId).subscribe(
			response => {
				this.toastrService.success('Operation successful!');
				this.getMeetings();
			},
			error => this.toastrService.error('Error when deleting meeting', error)
		);
	}

	public sortMeetings(option, unsorted) {
		switch (option) {
			case 'title':
				unsorted.sort(function(a, b) {
				  	return a.title === b.title ? 0 : +(a.title > b.title) || -1;
				});
				break;
			case 'speaker':
				unsorted.sort(function(a, b) {
					return a.speaker === b.speaker ? 0 : +(a.speaker > b.speaker) || -1;
				});
				break;
			case 'lastUpdate':
				unsorted.sort(function(a, b){
				  return a.lastUpdateDatetime === b.lastUpdateDatetime ? 0 : +(a.lastUpdateDatetime > b.lastUpdateDatetime) || -1;
				});
				break;
			case 'initDate':
				unsorted.sort(function(a, b){
				  return a.initDate === b.initDate ? 0 : +(a.initDate > b.initDate) || -1;
				});
				break;
			case 'inverse':
				unsorted.reverse();
				break;
		}
	}

	filterList(): Meeting[] {
		if (!this.filterBy || !this.filterValue)	{
				return this.meetings;
		}

		switch (this.filterBy) {
			case 'search': {
				const value = this.filterValue.toLowerCase();
				return this.meetings.filter(meeting => meeting.speaker.toLowerCase().includes(value) ||
					meeting.title.toLowerCase().includes(value) ||
					meeting.description.toLowerCase().includes(value));
			}
		}
  }

	open(content, id: number) {
		this.modalService.open(content).result.then((result) => {
    	if (result === 'delete') {
				this.deleteMeeting(id);
			}
    });
  }

  search() {
  	this.filterBy = 'search';
  	this.filterValue = this.searchValue;
	}

	canDelete(name: string): boolean {
    return this.authGuardService.canDelete(name);
  }

  canEdit(name: string): boolean {
    return this.authGuardService.canEdit(name);
  }
}
