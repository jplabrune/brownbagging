import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { MeetingListComponent } from './meeting-list.component';

import { Meeting } from '../meeting/meeting';
import { MeetingService } from '../meeting/meeting.service';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MdSelectModule } from '@angular/material';
import { TruncatePipe } from '../pipes/pipeTruncate.pipe';

import { RouterTestingModule } from '@angular/router/testing';

import { AuthGuardService } from '../shared/auth-guard.service';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('MeetingListComponent', () => {
  let component: MeetingListComponent;
  let meetingService: MeetingService;
  let toastrService: ToastrService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MeetingListComponent, TruncatePipe],
      providers: [MeetingService, AuthGuardService],
      imports: [FormsModule, HttpClientModule, RouterTestingModule, ToastrModule.forRoot(),
        NgbModule.forRoot(), MdSelectModule, BrowserAnimationsModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    const fixture = TestBed.createComponent(MeetingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    meetingService = fixture.debugElement.injector.get(MeetingService);
    toastrService = fixture.debugElement.injector.get(ToastrService);
  });

  describe('when initializing', () => {

    it('should load meetings and participants', () => {
      spyOn(meetingService, 'getMeetings').and.returnValue(
        Observable.of([{ id: '1', description: 'a meeting' }, { id: '1', description: 'another meeting' }]));

      component.ngOnInit();

      expect(component.meetings.length).toBe(2);
    });

    it('should check each meeting, if it can be deleted or edited by owner', () => {
      const response = {
        'name': 'userName',
        'role': 'user'
      };
      expect(component.canDelete('userName')).toBe(true);
      expect(component.canEdit('userName')).toBe(true);
    });

    it('should check each meeting, if it can be deleted or edited by admin', () => {
      const response = {
        'name': 'user',
        'role': 'admin'
      };
      expect(component.canDelete('userName')).toBe(true);
      expect(component.canEdit('userName')).toBe(true);
    });

    xit('should check each meeting, if it can\'t be deleted or edited by another users(except admin or owner)', () => {
      const response = {
        'name': 'user',
        'role': 'user'
      };
      expect(component.canDelete('userName')).toBe(false);
      expect(component.canEdit('userName')).toBe(false);
    });

    it('should display a toaster error when meetings can not be loaded', () => {
      spyOn(meetingService, 'getMeetings').and.returnValue(Observable.throw({ error: 'Error' }));
      spyOn(toastrService, 'error');

      component.ngOnInit();

      expect(toastrService.error).toHaveBeenCalledWith('Error when loading meetings', { error: 'Error' });
    });

    it('should display spinner when data is loading', () => {
      expect(component.showLoading).toBe(true);
    });

    it('should not display spinner when data is ready', () => {
      spyOn(meetingService, 'getMeetings').and.returnValue(
        Observable.of([{ id: '1', description: 'a meeting' }, { id: '1', description: 'another meeting' }]));

      component.ngOnInit();
      expect(component.showLoading).toBe(false);
    });
  });

  describe('when delete a meeting', () => {
    it('should display a toaster error when meeting not be exist', () => {
      spyOn(meetingService, 'deleteMeetingById').and.returnValue(Observable.throw({ error: 'Error' }));
      spyOn(toastrService, 'error');

      component.deleteMeeting('0');

      expect(toastrService.error).toHaveBeenCalledWith('Error when deleting meeting', { error: 'Error' });
    });

    it('should display a toaster notification when meeting was delete', () => {
      spyOn(meetingService, 'deleteMeetingById').and.returnValue(Observable.of([{}]));
      spyOn(toastrService, 'success');

      component.deleteMeeting('0');

      expect(toastrService.success).toHaveBeenCalledWith('Operation successful!');
    });
  });

  describe('When ordering the list of meetings', () => {
    const mockMeetings: Array<Meeting> = [
      new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
        'JuanPa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', []),
      new Meeting('00001', 'AWS-DynamoDB', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
        'Fede', new Date(2017, 7, 16, 16, 0, 0, 0), '2017-07-20T18:37:49.459Z', []),
      new Meeting('00003', 'Angular 2', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
        'Pablo', new Date(2017, 5, 16, 16, 0, 0, 0), '2017-08-20T18:37:49.459Z', []),
    ];

    it('must show it sorted by title', () => {
      const mockMeetings2: Array<Meeting> = [
        new Meeting('00001', 'AWS-DynamoDB', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Fede', new Date(2017, 7, 16, 16, 0, 0, 0), '2017-07-20T18:37:49.459Z', []),
        new Meeting('00003', 'Angular 2', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Pablo', new Date(2017, 5, 16, 16, 0, 0, 0), '2017-08-20T18:37:49.459Z', []),
        new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'JuanPa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', []),
      ];

      component.sortMeetings('title', mockMeetings);
      expect(mockMeetings).toEqual(mockMeetings2);
    });

    it('must show it sorted by speker name', () => {
      const mockMeetings2: Array<Meeting> = [
        new Meeting('00001', 'AWS-DynamoDB', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Fede', new Date(2017, 7, 16, 16, 0, 0, 0), '2017-07-20T18:37:49.459Z', []),
        new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'JuanPa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', []),
        new Meeting('00003', 'Angular 2', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Pablo', new Date(2017, 5, 16, 16, 0, 0, 0), '2017-08-20T18:37:49.459Z', []),
      ];

      component.sortMeetings('speaker', mockMeetings);
      expect(mockMeetings).toEqual(mockMeetings2);
    });

    it('must show it sorted by last update time', () => {
      const mockMeetings2: Array<Meeting> = [
        new Meeting('00003', 'Angular 2', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Pablo', new Date(2017, 5, 16, 16, 0, 0, 0), '2017-08-20T18:37:49.459Z', []),
        new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'JuanPa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', []),
        new Meeting('00001', 'AWS-DynamoDB', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Fede', new Date(2017, 7, 16, 16, 0, 0, 0), '2017-07-20T18:37:49.459Z', []),
      ];

      component.sortMeetings('lastUpdate', mockMeetings);
      expect(mockMeetings).toEqual(mockMeetings2);
    });

    it('must show it in descendant form', () => {
      const mockMeetings2: Array<Meeting> = [
        new Meeting('00001', 'AWS-DynamoDB', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Fede', new Date(2017, 7, 16, 16, 0, 0, 0), '2017-07-20T18:37:49.459Z', []),
        new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'JuanPa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', []),
        new Meeting('00003', 'Angular 2', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Pablo', new Date(2017, 5, 16, 16, 0, 0, 0), '2017-08-20T18:37:49.459Z', []),
      ];

      component.sortMeetings('inverse', mockMeetings);
      expect(mockMeetings).toEqual(mockMeetings2);
    });

    it('must show it sorted by meeting date', () => {
      const mockMeetings2: Array<Meeting> = [
        new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'JuanPa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', []),
        new Meeting('00001', 'AWS-DynamoDB', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Fede', new Date(2017, 7, 16, 16, 0, 0, 0), '2017-07-20T18:37:49.459Z', []),
        new Meeting('00003', 'Angular 2', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Pablo', new Date(2017, 5, 16, 16, 0, 0, 0), '2017-08-20T18:37:49.459Z', [])
      ];

      component.sortMeetings('initDate', mockMeetings);
      expect(mockMeetings).toEqual(mockMeetings2);
    });

  });

  describe('when filter the meeting list', () => {
    const mockMeetings: Array<Meeting> = [
      new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
        'JuanPa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', []),
      new Meeting('00001', 'AWS-DynamoDB', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
        'Fede', new Date(2017, 7, 16, 16, 0, 0, 0), '2017-07-20T18:37:49.459Z', []),
      new Meeting('00003', 'Angular 2', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
        'Pablo', new Date(2017, 5, 16, 16, 0, 0, 0), '2017-08-20T18:37:49.459Z', []),
    ];

    it('should search correct input string', () => {
      component.searchValue = 'name';
      component.search();
      expect(component.filterBy).toBe('search');
      expect(component.filterValue).toBe('name');
    });

    it('should display only the meetings that include a text substring in any attribute', () => {
      const mockResult = [
        new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'JuanPa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', [])
      ];

      component.meetings = mockMeetings;
      component.filterBy = 'search';
      component.filterValue = 'HelloWord';

      component.filterList();
      expect(component.filterList()).toEqual(mockResult);
    });

    it('should display all meetings in the list if search an empty string', () => {
      component.meetings = mockMeetings;
      component.filterBy = 'search';
      component.filterValue = '';

      component.filterList();
      expect(component.filterList()).toEqual(mockMeetings);
    });

    it('should display the empty list if the string is not included in any meetings', () => {
      component.meetings = mockMeetings;
      component.filterBy = 'search';
      component.filterValue = 'String';

      component.filterList();
      expect(component.filterList()).toEqual([]);
    });
  });
});
