import { TestBed, async, getTestBed } from '@angular/core/testing';

import { UserClaimsService } from './user-claims.service';

describe('UserClaimsService', () => {
  let userClaimsService: UserClaimsService;
  const oAuthService = {
    getIdentityClaims: jasmine.createSpy('getIdentityClaims').and.returnValue({
      email: 'test@email.com',
      username: 'username',
      role: 'role'
    })
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [UserClaimsService]
    });

    const testbed = getTestBed();
    userClaimsService = testbed.get(UserClaimsService);
  }));

  it('should set the user claims', () => {
    expect(userClaimsService.email).toBeUndefined();
    expect(userClaimsService.username).toBeUndefined();
    expect(userClaimsService.role).toBeUndefined();

    userClaimsService.setClaims();

    expect(userClaimsService.email).toBe('test@email.com');
    expect(userClaimsService.username).toBe('username');
    expect(userClaimsService.role).toBe('role');
  });
});
