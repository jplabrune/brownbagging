import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuardService {

  constructor() {}

  canEdit(meetingOwnerName: string): boolean {
    return true;
  }

  canDelete(meetingOwnerName: string): boolean {
    return true;
  }
}
