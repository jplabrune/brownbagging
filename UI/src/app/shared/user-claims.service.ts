import { Injectable } from '@angular/core';

@Injectable()
export class UserClaimsService {
  role: string;
  username: string;
  email: string;

  constructor() {}

  setClaims() {
    this.role = 'role';
    this.username = 'username';
    this.email = 'test@email.com';
  }
}
