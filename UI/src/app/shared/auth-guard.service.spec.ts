import { TestBed, async, getTestBed } from '@angular/core/testing';

import { AuthGuardService } from './auth-guard.service';

describe('AuthGuardService', () => {
  xdescribe('when no one is logged in', () => {
    let authGuardService: AuthGuardService;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        providers: [AuthGuardService]
      });

      const testbed = getTestBed();
      authGuardService = testbed.get(AuthGuardService);
    }));

    it('should can not edit any meeting', () => {
      expect(authGuardService.canEdit('test name')).toBe(false);
    });

    it('should can not delete any meeting', () => {
      expect(authGuardService.canDelete('test name')).toBe(false);
    });
  });

  describe('when user is log in', () => {

    let authGuardService: AuthGuardService;
    const oAuthService = {
      getIdentityClaims: jasmine.createSpy('getIdentityClaims').and.returnValue({
        email: 'test@email.com',
        name: 'test name',
        role: 'some role'
      })
    };

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        providers: [AuthGuardService]
      });

      const testbed = getTestBed();
      authGuardService = testbed.get(AuthGuardService);
    }));

    it('should can edit if user is the meeting\'s owner', () => {
      expect(authGuardService.canEdit('test name')).toBe(true);
    });

    xit('should can\'t edit if user isn\'t the meeting\'s owner', () => {
      expect(authGuardService.canEdit('any name')).toBe(false);
    });

    it('should can delete if user is the meeting\'s owner', () => {
      expect(authGuardService.canDelete('test name')).toBe(true);
    });

    xit('should can\'t delete if user isn\'t the meeting\'s owner', () => {
      expect(authGuardService.canDelete('any name')).toBe(false);
    });
  });

  describe('when admin is log in', () => {
    let authGuardService: AuthGuardService;
    const oAuthService = {
      getIdentityClaims: jasmine.createSpy('getIdentityClaims').and.returnValue({
        email: 'test@email.com',
        name: 'test name',
        role: 'admin'
      })
    };

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        providers: [AuthGuardService]
      });

      const testbed = getTestBed();
      authGuardService = testbed.get(AuthGuardService);
    }));

    it('should can edit if user is admin', () => {
      expect(authGuardService.canEdit('test name')).toBe(true);
    });

    it('should can delete if user is admin', () => {
      expect(authGuardService.canDelete('test name')).toBe(true);
    });
  });
});
