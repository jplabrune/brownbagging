import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { environment } from '../../environments/environment';

@Injectable()
export class BannerService {

  constructor(private http: Http, private sanitizer: DomSanitizer) { }

  loadAvatar(username): Observable<any> {
    const serviceUrl = environment.servicesUrl.getAvatar.replace('<username>', username);
    const headers = new Headers({ 'Content-Type': 'image/jpg' });
    const options = new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob });

		return this.http.get(serviceUrl, options)
      .map(res => res.blob()).map(blob => {
        const urlCreator = window.URL;
        return this.sanitizer.bypassSecurityTrustUrl(urlCreator.createObjectURL(blob));
      })
      .catch(error => Observable.throw(error));
	}
}
