import { TestBed, async, getTestBed } from '@angular/core/testing';
import { BaseRequestOptions, Response, ResponseOptions, ResponseType, Http, XHRBackend } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BannerService } from './banner.service';

describe('BannerService', () => {
  let bannerService: BannerService;
  let backend: MockBackend;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        BannerService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (mockBackend: MockBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(mockBackend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions],
        }
      ]
    });

    const testbed = getTestBed();
    backend = testbed.get(MockBackend);
    bannerService = testbed.get(BannerService);
  }));

  function setupConnections(mockBackend: MockBackend, options: any) {
    mockBackend.connections.subscribe((connection: MockConnection) => {
      const responseOptions = new ResponseOptions(options);
      const response = new Response(responseOptions);

      if (response.ok) {
        connection.mockRespond(response);
      } else {
        connection.mockError(response as any as Error);
      }
    });
  }

  describe('when calling load avatar', () => {
    it('should throw an error uf service fails', () => {
      setupConnections(backend, { status: 404, type: ResponseType.Error });

      bannerService.loadAvatar(688748).subscribe(
        null,
        error => {
          expect(error.status).toBe(404);
      });
    });
  });
});
