import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BannerService } from './banner.service';
import { UserClaimsService } from '../shared/user-claims.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent {
  @Output() isOpen = new EventEmitter<boolean>();

  avatarImageLoaded: Boolean = false;
  avatarImagePath: String = null;
  unknownAvatarPath: String = './assets/avatar_images/unknown_avatar.jpg';

  constructor(private bannerService: BannerService,
    private userClaimsService: UserClaimsService, private toastrService: ToastrService) {
  }

  loadAvatar() {
    this.bannerService.loadAvatar(this.userClaimsService.username).subscribe(
      response => {
        this.avatarImagePath = response;
        this.avatarImageLoaded = true;
      },
      error => {
        this.toastrService.error(error);
        this.avatarImageLoaded = false;
      }
    );
  }
  navOpen() {
    this.isOpen.emit(true);
  }
}
