import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';

import { BannerComponent } from './banner.component';
import { BannerService } from './banner.service';
import { UserClaimsService } from '../shared/user-claims.service';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { AvatarModule } from 'ngx-avatar';

describe('BannerComponent', () => {
  let component: BannerComponent;
  let fixture: ComponentFixture<BannerComponent>;
  let bannerService: BannerService;
  let toastrService: ToastrService;
  const userClaimsService = {
    username: 'name'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerComponent ],
      providers: [ BannerService, { provide: UserClaimsService, useValue: userClaimsService }, ToastrService ],
      imports: [ HttpModule, ToastrModule.forRoot(), AvatarModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    bannerService = fixture.debugElement.injector.get(BannerService);
    toastrService = fixture.debugElement.injector.get(ToastrService);
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
    expect(component.avatarImageLoaded).toBeFalsy();
    expect(component.avatarImagePath).toBeNull();
    expect(component.unknownAvatarPath).toBe('./assets/avatar_images/unknown_avatar.jpg');
  });

  describe('when loading avatar', () => {
    it('should call service and set user avatar image property', () => {
      spyOn(bannerService, 'loadAvatar').and.returnValue(Observable.of('avatar image data'));

      component.loadAvatar();

      expect(bannerService.loadAvatar).toHaveBeenCalledWith('name');
      expect(component.avatarImagePath).toBe('avatar image data');
      expect(component.avatarImageLoaded).toBeTruthy();
    });

    it('should display an error if service fails', () => {
      spyOn(bannerService, 'loadAvatar').and.returnValue(Observable.throw('Error'));
      spyOn(toastrService, 'error');
      component.loadAvatar();

      expect(bannerService.loadAvatar).toHaveBeenCalledWith('name');
      expect(component.avatarImagePath).toBeNull();
      expect(toastrService.error).toHaveBeenCalledWith('Error');
      expect(component.avatarImageLoaded).toBeFalsy();
    });
  });

  describe('when try to open side-nav', () => {
    it('should open it when click the button', () => {
      spyOn(component.isOpen, 'emit').and.returnValue(true);
      component.navOpen();
      expect(component.isOpen.emit).toHaveBeenCalledWith(true);
    });
  });
});
