import { Component, OnInit, ViewChild } from '@angular/core';
import { MeetingListComponent } from './meeting-list/meeting-list.component';

import { environment } from '../environments/environment';
import { UserClaimsService } from './shared/user-claims.service';
import { BannerService } from './banner/banner.service';
import { BannerComponent } from './banner/banner.component';
import { MdSidenavModule, MdSidenav } from '@angular/material';

@Component({
  selector: 'app-init',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  @ViewChild(BannerComponent)
  bannerComponent: BannerComponent;
  @ViewChild('sidenav') public myNav: MdSidenav;

  constructor(private userClaimsService: UserClaimsService,
    private bannerService: BannerService, sideNav: MdSidenavModule) { }

  ngOnInit() {
    // Aqui deberia implementarse la autenticacion, por ej. OAuth2
  }
}
