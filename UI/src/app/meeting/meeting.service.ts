import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';

import { Meeting } from './meeting';
import { environment } from '../../environments/environment';

@Injectable()
export class MeetingService {
  private getMeetingsUrl = environment.servicesUrl.getMeetings;
  private deleteMeetingUrl = environment.servicesUrl.deleteMeeting;
  private createMeetingUrl = environment.servicesUrl.createMeeting;
  private getMeetingByIdUrl = environment.servicesUrl.getMeetingById;
  private updateMeetingUrl = environment.servicesUrl.updateMeeting;
  private getMeetingVotesUrl = environment.servicesUrl.getMeetingVotes;
  private voteMeetingUrl = environment.servicesUrl.voteMeeting;
  private cancelVoteMeetingUrl = environment.servicesUrl.cancelVoteMeeting;

  private httpOption = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  constructor(private http: HttpClient) { }

  getMeetings(): Observable<Meeting[]> {
    return this.http.get(this.getMeetingsUrl)
      .map(response => response as Meeting[])
      .catch(error => Observable.throw(error));
  }

  getMeetingById(id): Observable<Meeting> {
    return this.http.get(this.getMeetingByIdUrl.replace('{id}', id))
      .map(response => response)
      .catch(error => Observable.throw(error));
  }

  getMeetingVotes(id: string): Observable<string> {
    return this.http.get(this.getMeetingVotesUrl.replace('{id}', id))
      .map(response => response)
      .catch(error => Observable.throw(error));
  }

  createMeeting(meeting: Meeting) {
    if (environment.useJson) {
      return Observable.of('');
    }

    const max = 2147483647;
    const min = 1;
    meeting.id = Math.random() * (max - min) + min;
    meeting.votes = '';
    return this.http.post(this.createMeetingUrl, meeting.toJson(), this.httpOption)
      .catch(error => Observable.throw(error));
  }

  updateMeeting(meeting: Meeting): Observable<string> {
    if (environment.useJson) {
      return Observable.of('');
    }

    return this.http.put(this.updateMeetingUrl.replace('{id}', meeting.id), meeting, this.httpOption)
      .catch(error => Observable.throw(error));
  }

  deleteMeetingById(meetingId: string): Observable<string> {
    if (environment.useJson) {
      return Observable.of('');
    }

    const deleteUrl = this.deleteMeetingUrl.replace('{id}', meetingId);
    return this.http.delete(deleteUrl, this.httpOption)
      .catch(error => Observable.throw(error));
  }

  voteMeeting(meetingId: string, username): Observable<string> {

    return Observable.of('');
    // const params = { 'meetingId': meetingId, 'memberKey': username };
    // if (environment.useJson) {
    //   return Observable.of('');
    // }
    // return this.http.post(this.voteMeetingUrl, params, this.httpOption)
    //   .catch(error => Observable.throw(error));
  }

  cancelVoteMeeting(meetingId: string, username): Observable<string> {
    return Observable.of('');
    // const params = { 'meetingId': meetingId, 'memberKey': username };
    // if (environment.useJson) {
    //   return Observable.of('');
    // }

    // return this.http.post(this.cancelVoteMeetingUrl, params, this.httpOption)
    //   .catch(error => Observable.throw(error));
  }
}
