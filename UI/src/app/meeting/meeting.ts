export class Meeting {
	constructor(public id, public title, public description, public speaker, public lastUpdateDatetime, public initDate, public votes) { }

	public toJson() {
		return {
			id: this.id,
			title: this.title,
			description: this.description,
			speaker: this.speaker,
			lastUpdateDatetime: this.lastUpdateDatetime,
			initDate: this.initDate,
			votes: this.votes
		};
	}
}
