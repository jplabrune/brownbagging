// import { TestBed, async, getTestBed } from '@angular/core/testing';
// import { BaseRequestOptions, Response, ResponseOptions, ResponseType, Http, XHRBackend } from '@angular/http';
// import { MockBackend, MockConnection } from '@angular/http/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';

import { MeetingService } from '../meeting/meeting.service';
import { Meeting } from '../meeting/meeting';
import { environment } from '../../environments/environment';

describe('MeetingService', () => {
  let injector: TestBed;
  let service: MeetingService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingService],
      imports: [HttpClientTestingModule]
    });

    injector = getTestBed();
    service = injector.get(MeetingService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('when getting all meetings', function () {
    it('should return Items property on success response', () => {
      const mockMeetings: Array<Meeting> = new Array();
      mockMeetings.push(new Meeting(
        'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
        '1', 'Fede', 'AWS-DynamoDB', '2017-05-18T10:20+01:00', '2017-07-20T18:37:49.459Z', ''
      ));
      mockMeetings.push(new Meeting(
        'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
        '2', 'FerDi', 'AWS-API Gateway', '2017-05-18T10:20+01:00', '2017-07-20T18:37:49.459Z', ''
      ));

      service.getMeetings().subscribe(
        data => { expect(data).toEqual(mockMeetings); }
      );

      const req = httpMock.expectOne('assets/mock_files/mock-meetings.json');
      expect(req.request.method).toBe('GET');
      req.flush(mockMeetings);
    });

    it('should return error message if call fails', () => {
      service.getMeetings().subscribe(
        null,
        error => {
          expect(error.statusText).toBe('Error while getting meetings');
        });

      const req = httpMock.expectOne('assets/mock_files/mock-meetings.json');
      req.flush(null, { status: 404, statusText: 'Error while getting meetings' });
    });
  });

  describe('when create new-meeting', function () {
    it('should return success response if service is working', () => {
      const item = new Meeting(1, 'title', 'description', 'name', '2017-05-10T10:20+01:00', '2017-07-20T18:37:49.459Z', []);
      environment.useJson = false;

      service.createMeeting(item).subscribe(
        response => {
          expect(response.body).toEqual('');
        }
      );

      const req = httpMock.expectOne('/');
      expect(req.request.method).toBe('POST');
    });

    it('should return error response if call fails', () => {
      const item = new Meeting(1, 'title', 'description', 'name', '2017-05-10T10:20+01:00', '2017-07-20T18:37:49.459Z', []);

      service.createMeeting(item).subscribe(
        null,
        error => {
          expect(error.statusText).toBe('Error while save new-meeting');
        });

      const req = httpMock.expectOne('/');
      req.flush(null, { status: 404, statusText: 'Error while save new-meeting' });
    });
  });

  describe('when getting a meeting by Id', function () {
    it('should return a meeting', () => {
      const item = new Meeting(1, 'title', 'description', 'name', '2017-05-10T10:20+01:00', '2017-07-20T18:37:49.459Z', []);

      service.getMeetingById('1').subscribe(
        data => { expect(data).toEqual(item); }
      );

      const req = httpMock.expectOne('assets/mock_files/mock-meeting_1.json');
      expect(req.request.method).toBe('GET');
      req.flush(item);
    });

    it('should return error message if call fails', () => {

      service.getMeetingById('1').subscribe(
        null,
        error => {
          expect(error.statusText).toBe('Error while try to get a meeting by id');
        });

      const req = httpMock.expectOne('assets/mock_files/mock-meeting_1.json');
      req.flush(null, { status: 404, statusText: 'Error while try to get a meeting by id' });
    });
  });

  describe('when deleting meeting', function () {
    it('should return success message if meeting was delete', () => {
      const item = new Meeting(1, 'title', 'description', 'name', '2017-05-10T10:20+01:00', '2017-07-20T18:37:49.459Z', []);

      service.deleteMeetingById('1').subscribe(
        response => { expect(true).toBe(true); },
        error => { expect(false).toBe(true); }
      );
      const req = httpMock.expectOne('/');
      expect(req.request.method).toBe('DELETE');
    });

    it('should return error message if call fails', () => {
      service.deleteMeetingById('1').subscribe(
        null,
        error => {
          expect(error.statusText).toBe('Error while deleting meeting');
        });

      const req = httpMock.expectOne('/');
      req.flush(null, { status: 404, statusText: 'Error while deleting meeting' });
    });
  });

  describe('when update meeting', function () {
    const item = new Meeting(1, 'title', 'description', 'name', '2017-05-10T10:20+01:00', '2017-07-20T18:37:49.459Z', []);

    it('should return success message if meeting was update', () => {
      service.updateMeeting(item).subscribe(
        response => { expect(true).toBe(true); },
        error => { expect(false).toBe(true); }
      );

      const req = httpMock.expectOne('/');
      expect(req.request.method).toBe('PUT');
    });

    it('should return error message if call fails', () => {
      service.updateMeeting(item).subscribe(
        null,
        error => {
          expect(error.statusText).toBe('Error while deleting meeting');
        });

      const req = httpMock.expectOne('/');
      req.flush(null, { status: 404, statusText: 'Error while deleting meeting' });
    });
  });

  // describe('when getting a list of members that voted meeting ', function () {
  //   it('should a list of people keys of member that has voted the meeting', () => {
  //     const peopleKeyFromHowHasVoted = '[666]';
  //     setupConnections(backend, { body: peopleKeyFromHowHasVoted, status: 200 });
  //     service.getMeetingVotes('1').subscribe(
  //       data => { expect(JSON.stringify(data)).toEqual('[666]'); }
  //     );
  //   });

  //   it('should return error message if call fails', () => {
  //     setupConnections(backend, { body: { message: 'Error while try to get a meeting by id' }, status: 404, type: ResponseType.Error });

  //     service.getMeetingVotes('1').subscribe(
  //       null,
  //       error => {
  //         expect(error.message).toBe('Error while try to get a meeting by id');
  //       });
  //   });
  // });

  // describe('when vote a meeting', function () {
  //   it('should return success response if service is working', () => {
  //     const meetingId = '0';
  //     const username = 666;
  //     setupConnections(backend, { body: {}, status: 200 });
  //     service.voteMeeting(meetingId, username).subscribe(
  //       response => { expect(response).toBe(''); },
  //       error => { expect(false).toBe(true); }
  //     );
  //   });

  //   it('should return error response if call fails', () => {
  //     const meetingId = '0';
  //     const username = 666;
  //     setupConnections(backend, { body: { message: 'Error while vote meeting' }, status: 404, type: ResponseType.Error });
  //     service.voteMeeting(meetingId, username).subscribe(
  //       null,
  //       error => {
  //         expect(error.message).toBe('Error while vote meeting');
  //       });
  //   });
  // });

  // describe('when cancel a meeting vote', function () {
  //   it('should return success response if service is working', () => {
  //     const meetingId = '0';
  //     const username = 666;
  //     setupConnections(backend, { body: {}, status: 200 });
  //     service.cancelVoteMeeting(meetingId, username).subscribe(
  //       response => {
  //         expect(response).toBe('');
  //       },
  //       error => { expect(false).toBe(true); }
  //     );
  //   });

  //   it('should return error response if call fails', () => {
  //     const meetingId = '0';
  //     const username = 666;
  //     setupConnections(backend, { body: { message: 'Error while cancel a meeting vote' }, status: 404, type: ResponseType.Error });
  //     service.cancelVoteMeeting(meetingId, username).subscribe(
  //       null,
  //       error => {
  //         expect(error.message).toBe('Error while cancel a meeting vote');
  //       });
  //   });
  // });
});
