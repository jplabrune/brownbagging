import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { MeetingListComponent } from './meeting-list/meeting-list.component';
import { NewMeetingComponent } from './new-meeting/new-meeting.component';
import { EditMeetingComponent } from './edit-meeting/edit-meeting.component';
import { MeetingDetailComponent } from './meeting-detail/meeting-detail.component';
import { SpeakerMeetingsComponent } from './speaker-meetings/speaker-meetings.component';

const routes: Routes = [
  { path: '', redirectTo: 'meeting-list', pathMatch: 'full' },
  { path: 'meeting-list', component: MeetingListComponent },
  { path: 'new-meeting', component: NewMeetingComponent },
  { path: 'edit-meeting/:id', component: EditMeetingComponent },
  { path: 'meeting-detail/:id', component: MeetingDetailComponent },
  { path: 'speaker-meetings/:username', component: SpeakerMeetingsComponent },
  { path: '**', redirectTo: 'meeting-list' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
