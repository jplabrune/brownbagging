import { TruncatePipe } from './pipeTruncate.pipe';

describe('Truncate Pipe', () => {

  let pipe: TruncatePipe;

  beforeEach(() => {
    pipe = new TruncatePipe();
  });

    it('must truncate the input value when its length is greater than the value passed by parameter', () => {
      expect(pipe.transform('hello world', ['5'])).toBe('hello...');
    });

    it('musnt truncate the input value when its length is less than the value passed by parameter', () => {
      expect(pipe.transform('hello world', ['15'])).toBe('hello world');
    });
});
