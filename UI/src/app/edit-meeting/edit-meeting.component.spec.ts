import { async, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { HttpClientModule } from '@angular/common/http';

import { EditMeetingComponent } from './edit-meeting.component';
import { MeetingService } from '../meeting/meeting.service';
import { Meeting } from '../meeting/meeting';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuardService } from '../shared/auth-guard.service';
import { NgxTypeaheadModule } from 'ngx-typeahead';

describe('Edit-Meeting Component', () => {
  let component: EditMeetingComponent;
  let meetingService: MeetingService;
  let toastrService: ToastrService;
  let authGuardService: AuthGuardService;
  const router = { navigate: jasmine.createSpy('navigate') };
  const activatedRoute = { params: Observable.of({id: 123}) };
  const mockMeeting = [
    {
      'description': 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
      'id': '1',
      'speaker': 'JuanPa',
      'title': 'AWS-DynamoDB',
      'lastUpdateDatetime': '2017-05-18T10:20+01:00',
      'initDate': '2017-07-20T18:37:49.459Z'
    }];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMeetingComponent ],
      providers: [ MeetingService, { provide: ActivatedRoute, useValue: activatedRoute },
        { provide: Router, useValue: router }, AuthGuardService ],
      imports: [ FormsModule, HttpClientModule, ToastrModule.forRoot(), NgbModule.forRoot(), NgxTypeaheadModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    const fixture = TestBed.createComponent(EditMeetingComponent);
    component = fixture.componentInstance;
    meetingService = fixture.debugElement.injector.get(MeetingService);
    toastrService = fixture.debugElement.injector.get(ToastrService);
    authGuardService = fixture.debugElement.injector.get(AuthGuardService);
  });

  it('should create component', () => {
    expect(component.jsonDataUrl).toBe('assets/mock_files/people.json');
    expect(component.model).toEqual(new Meeting('', '', '', '', '', '', []));
    expect(component.showLoading).toBeTruthy();
    expect(component.datePicker).toBeUndefined();
    expect(component.time).toBeUndefined();
  });

  describe('when initializing', () => {
    it('should show toaster error if current user is not admin or meeting\'s owner', () => {
      spyOn(meetingService, 'getMeetingById').and.returnValue(Observable.of(mockMeeting));
      spyOn(authGuardService, 'canEdit').and.returnValue(false);
      spyOn(toastrService, 'error');

      component.ngOnInit();

      expect(toastrService.error).toHaveBeenCalledWith('This operation requires owner or admin permissions');
    });

    it('should show/hide spinner while meeting is being loaded', () => {
      expect(component.showLoading).toBe(true);
      spyOn(meetingService, 'getMeetingById').and.returnValue(Observable.of(mockMeeting));
      spyOn(authGuardService, 'canEdit').and.returnValue(false);

      component.ngOnInit();

      expect(component.showLoading).toBe(false);
    });

    it('should get meeting by id', () => {
      spyOn(meetingService, 'getMeetingById');
      spyOn(meetingService, 'updateMeeting').and.returnValue(Observable.of([{}]));

      component.ngOnInit();

      expect(meetingService.getMeetingById).toHaveBeenCalledWith(123);
    });

    it('should load date variables with correct value if it is not undefined', () => {
      const mock = new Meeting(1, 'title', 'description', 'speaker', new Date(), new Date('2017-07-20T18:37:49.459Z'), []);
      const datePicker = {year: mock.initDate.getFullYear(), month: mock.initDate.getMonth() + 1, day: mock.initDate.getDate()};
      const time = {hour: mock.initDate.getHours(), minute: mock.initDate.getMinutes()};

      spyOn(meetingService, 'getMeetingById').and.returnValue(Observable.of(mock));
      spyOn(meetingService, 'updateMeeting').and.returnValue(Observable.of([{}]));
      spyOn(authGuardService, 'canEdit').and.returnValue(true);

      component.ngOnInit();

      expect(component.datePicker).toEqual(datePicker);
      expect(component.time).toEqual(time);
    });

    it('should load date variables with correct value if it is undefined', () => {
      const mock = new Meeting(1, 'title', 'description', 'speaker', new Date(), '', []);

      spyOn(meetingService, 'getMeetingById').and.returnValue(Observable.of(mock));
      spyOn(meetingService, 'updateMeeting').and.returnValue(Observable.of([{}]));
      spyOn(authGuardService, 'canEdit').and.returnValue(false);

      component.ngOnInit();
      component.speakerSelected('name');
      expect(component.model.speaker).toBe('name');
      expect(component.typeAheadSpeaker).toBe('name');
      component.model.speaker = ' ';
      component.verifyUser();
      expect(component.model.speaker).toBe('');
      expect(component.typeAheadSpeaker).toBe('');

      expect(component.datePicker).toBeUndefined();
      expect(component.time).toBeUndefined();
    });
  });

  describe('when updating a meeting', () => {
    it('should display a toaster sucess message if everything goes ok', () => {
      spyOn(meetingService, 'updateMeeting').and.returnValue(Observable.of([{}]));
      spyOn(toastrService, 'success');

      component.onSubmit();

      expect(meetingService.updateMeeting).toHaveBeenCalledWith(jasmine.any(Meeting));
      expect(toastrService.success).toHaveBeenCalledWith('Operation successful!');
      expect(router.navigate).toHaveBeenCalledWith(['meeting-list']);
    });

    it('should set time in 00:00 if its undefined', () => {
      spyOn(meetingService, 'updateMeeting').and.returnValue(Observable.of([{}]));
      spyOn(toastrService, 'success');

      expect(component.datePicker).toBeUndefined();
      component.ngOnInit();

      const today = new Date();
      component.datePicker = {year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate()};

      component.onSubmit();

      const date = new Date(component.datePicker.year, component.datePicker.month - 1, component.datePicker.day,
        0, 0, 0, 0);

      expect(component.model.initDate).toEqual(date);
      expect(meetingService.updateMeeting).toHaveBeenCalledWith(jasmine.any(Meeting));
      expect(toastrService.success).toHaveBeenCalledWith('Operation successful!');
      expect(router.navigate).toHaveBeenCalledWith(['meeting-list']);
    });

    it('should display a toaster error if service fails', () => {
      spyOn(meetingService, 'updateMeeting').and.returnValue(Observable.throw({error: 'Error'}));
      spyOn(toastrService, 'error');

      component.onSubmit();

      expect(meetingService.updateMeeting).toHaveBeenCalledWith(jasmine.any(Meeting));
      expect(toastrService.error).toHaveBeenCalledWith('Error when update the meeting', {error: 'Error'});
    });
   });

  it('should set current date and time when selecting today date', () => {
    const today = new Date();
    component.selectToday();

    expect(component.datePicker.day).toBe(today.getDate());
    expect(component.datePicker.month).toBe(today.getMonth() + 1);
    expect(component.datePicker.year).toBe(today.getFullYear());
  });

  it('should set speaker in model when a speaker is seleted', () => {
    component.speakerSelected('test');

    expect(component.model.speaker).toBe('test');
  });
});
