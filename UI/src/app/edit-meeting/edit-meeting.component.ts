import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router  } from '@angular/router';
import { ToastrService, ToastrConfig } from 'ngx-toastr';
import 'rxjs/add/operator/switchMap';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { Meeting } from '../meeting/meeting';
import { MeetingService } from '../meeting/meeting.service';
import { AuthGuardService } from '../shared/auth-guard.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-edit-meeting',
  templateUrl: './edit-meeting.component.html',
  styleUrls: ['./edit-meeting.component.css'],
})
export class EditMeetingComponent implements OnInit {
  model: Meeting = new Meeting('', '', '', '', '', '', []);
  showLoading: Boolean = true;
  datePicker: NgbDateStruct;
  time;
  jsonDataUrl: String = environment.servicesUrl.getPeople;
  typeAheadSpeaker: any = '';

  constructor(private activateRoute: ActivatedRoute, private meetingService: MeetingService,
    private toastrService: ToastrService, private router: Router, private authGuardService: AuthGuardService) { }

  ngOnInit() {
  	this.activateRoute.params.switchMap((params: Params) =>
      this.meetingService.getMeetingById(params['id'])).subscribe(model => {
        this.model = model;
        if (!this.authGuardService.canEdit(this.model.speaker)) {
          this.showLoading = false;
          this.router.navigate(['meeting-list']);
          this.toastrService.error('This operation requires owner or admin permissions');
          return;
        }
        this.showLoading = false;
        this.typeAheadSpeaker = model.speaker;
        if (model.initDate) {
          const date = new Date(model.initDate);
          this.datePicker = {year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate()};
          this.time = {hour: date.getHours(), minute: date.getMinutes()};
        }
      },
      error => {
        this.toastrService.error('Error while loading the meeting');
        this.showLoading = false;
        this.router.navigate(['meeting-list']);
      }
    );
  }

  onSubmit() {
    if (!this.datePicker) {
      this.model.initDate = undefined;
    } else {
      if (!this.time) {
        this.time = {hour: 0, minute: 0};
      }
      const date = new Date(this.datePicker.year, this.datePicker.month - 1, this.datePicker.day, this.time.hour, this.time.minute, 0, 0);
      this.model.initDate = date;
    }

    this.meetingService.updateMeeting(this.model).subscribe(
      updateMeeting => {
        this.toastrService.success('Operation successful!');
        this.router.navigate(['meeting-list']);
      },
      error => this.toastrService.error('Error when update the meeting', error)
    );
  }

  selectToday() {
    const today = new Date();
    this.datePicker = {year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate()};
  }

  speakerSelected(speaker) {
    this.typeAheadSpeaker = speaker;
    this.model.speaker = speaker;
  }

  verifyUser() {
    if (this.typeAheadSpeaker !== this.model.speaker) {
      this.typeAheadSpeaker = '';
      this.model.speaker = '';
    }
  }
}
