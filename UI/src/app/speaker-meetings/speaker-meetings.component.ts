import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToastrService, ToastrConfig } from 'ngx-toastr';

import { Meeting } from '../meeting/meeting';
import { MeetingService } from '../meeting/meeting.service';

@Component({
  selector: 'app-speaker-meetings',
  templateUrl: './speaker-meetings.component.html',
	styleUrls: ['./speaker-meetings.component.css']
})
export class SpeakerMeetingsComponent implements OnInit {
  meetings:  Array<Meeting> = [];
	speaker: string;
	showLoading: Boolean = true;
	valueSort = '';

  constructor(
		private meetingService: MeetingService,
		private activateRoute: ActivatedRoute,
		private toastrService: ToastrService
	) { }

  ngOnInit() {
    this.getModel();
		this.getMeetings();
	}

	public getMeetings() {
		this.meetingService.getMeetings().subscribe(
			meetingsResponse => {
				this.meetings = meetingsResponse.filter(meeting => meeting.speaker === this.speaker);
				this.showLoading = false;
			},
			error => this.toastrService.error('Error when loading meetings', error)
		);
  }

  getModel() {
		this.activateRoute.params.switchMap((params: Params) => this.speaker = params['username']).subscribe();
	}

	public sortMeetings(option) {
		if (option === this.valueSort) {
			option = 'inverse';
		} else {
			this.valueSort = option;
		}

		switch (option) {
			case 'title':
				this.meetings.sort(function(a, b) {
				  	return a.title === b.title ? 0 : +(a.title > b.title) || -1;
				});
				break;
			case 'lastUpdate':
				this.meetings.sort(function(a, b){
				  return a.lastUpdateDatetime === b.lastUpdateDatetime ? 0 : +(a.lastUpdateDatetime > b.lastUpdateDatetime) || -1;
				});
				break;
			case 'initDate':
				this.meetings.sort(function(a, b){
				  return a.initDate === b.initDate ? 0 : +(a.initDate > b.initDate) || -1;
				});
				break;
			case 'inverse':
				this.meetings.reverse();
				break;
		}
	}
}
