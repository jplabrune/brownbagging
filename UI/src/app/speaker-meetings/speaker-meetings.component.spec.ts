import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { SpeakerMeetingsComponent } from './speaker-meetings.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { TruncatePipe } from '../pipes/pipeTruncate.pipe';

import { MeetingService } from '../meeting/meeting.service';
import { Meeting } from '../meeting/meeting';

import { ActivatedRoute } from '@angular/router';

describe('SpeakerMeetingsComponent', () => {
  let component: SpeakerMeetingsComponent;
  let meetingService: MeetingService;
  let toastrService: ToastrService;
  const mockMeetings: Array<Meeting> = [
    new Meeting(1, 'title1', 'description1', 'speaker1', 'date1', 'date1', []),
    new Meeting(2, 'title2', 'description2', 'speaker2', 'date2', 'date2', [])

  ];

  const successSwitchMap = Observable.of({ username: 'speakerName' });
  const activatedRoute = {
    params: { switchMap: jasmine.createSpy('').and.returnValue(successSwitchMap) }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SpeakerMeetingsComponent, TruncatePipe],
      providers: [MeetingService, { provide: ActivatedRoute, useValue: activatedRoute }],
      imports: [FormsModule, HttpClientModule, RouterTestingModule, ToastrModule.forRoot()]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    const fixture = TestBed.createComponent(SpeakerMeetingsComponent);
    component = fixture.componentInstance;
    meetingService = fixture.debugElement.injector.get(MeetingService);
    toastrService = fixture.debugElement.injector.get(ToastrService);
  });

  describe('when initialize', () => {

    it('should display spinner when data is loading', () => {
      expect(component.showLoading).toBe(true);
    });

    it('should not display spinner when data is ready', () => {
      spyOn(meetingService, 'getMeetings').and.returnValue(Observable.of(mockMeetings));
      component.speaker = 'speaker';
      component.getMeetings();
      expect(component.showLoading).toBe(false);
    });

    it('should show user meetings when date is ready', () => {
      const item = [new Meeting(2, 'title2', 'description2', 'speaker2', 'date2', 'date2', [])];

      spyOn(meetingService, 'getMeetings').and.returnValue(Observable.of(mockMeetings));
      component.speaker = 'speaker2';
      component.getMeetings();
      expect(component.meetings).toEqual(item);
    });

    it('should display a toaster error when meetings can not be loaded', () => {
      spyOn(meetingService, 'getMeetings').and.returnValue(Observable.throw({ error: 'Error' }));
      spyOn(toastrService, 'error');

      component.ngOnInit();

      expect(toastrService.error).toHaveBeenCalledWith('Error when loading meetings', { error: 'Error' });
    });

    it('should display speaker name', () => {

      component.getModel();

      expect(activatedRoute.params.switchMap).toHaveBeenCalled();
    });
  });

  describe('When ordering the list of meetings', () => {
    const arrayMeetings = [
      new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
        'Juampa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', []),
      new Meeting('00001', 'AWS-DynamoDB', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
        'Juampa', new Date(2017, 7, 16, 16, 0, 0, 0), '2017-07-20T18:37:49.459Z', []),
      new Meeting('00003', 'Angular 2', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
        'Juampa', new Date(2017, 5, 16, 16, 0, 0, 0), '2017-08-20T18:37:49.459Z', []),
    ];
    beforeEach(() => {
      component.meetings = arrayMeetings;
    });

    it('must show it sorted by title', () => {
      const mockMeetings2: Array<Meeting> = [
        new Meeting('00001', 'AWS-DynamoDB', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Juampa', new Date(2017, 7, 16, 16, 0, 0, 0), '2017-07-20T18:37:49.459Z', []),
        new Meeting('00003', 'Angular 2', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Juampa', new Date(2017, 5, 16, 16, 0, 0, 0), '2017-08-20T18:37:49.459Z', []),
        new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Juampa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', []),
      ];

      component.sortMeetings('title');
      expect(component.meetings).toEqual(mockMeetings2);
    });

    it('must show it sorted by last update time', () => {
      const mockMeetings2: Array<Meeting> = [
        new Meeting('00003', 'Angular 2', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Juampa', new Date(2017, 5, 16, 16, 0, 0, 0), '2017-08-20T18:37:49.459Z', []),
        new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Juampa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', []),
        new Meeting('00001', 'AWS-DynamoDB', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Juampa', new Date(2017, 7, 16, 16, 0, 0, 0), '2017-07-20T18:37:49.459Z', []),
      ];

      component.sortMeetings('lastUpdate');
      expect(component.meetings).toEqual(mockMeetings2);
    });

    it('must show it sorted by meeting date', () => {
      const mockMeetings2: Array<Meeting> = [
        new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Juampa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', []),
        new Meeting('00001', 'AWS-DynamoDB', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Juampa', new Date(2017, 7, 16, 16, 0, 0, 0), '2017-07-20T18:37:49.459Z', []),
        new Meeting('00003', 'Angular 2', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Juampa', new Date(2017, 5, 16, 16, 0, 0, 0), '2017-08-20T18:37:49.459Z', [])
      ];

      component.sortMeetings('initDate');
      expect(component.meetings).toEqual(mockMeetings2);
    });

    it('must show it sorted by title but in descendant form', () => {
      const mockMeetings2: Array<Meeting> = [
        new Meeting('00002', 'HelloWord', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Juampa', new Date(2017, 6, 16, 16, 0, 0, 0), '2017-06-20T18:37:49.459Z', []),
        new Meeting('00003', 'Angular 2', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Juampa', new Date(2017, 5, 16, 16, 0, 0, 0), '2017-08-20T18:37:49.459Z', []),
        new Meeting('00001', 'AWS-DynamoDB', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
          'Juampa', new Date(2017, 7, 16, 16, 0, 0, 0), '2017-07-20T18:37:49.459Z', [])
      ];
      component.sortMeetings('title');
      component.sortMeetings('title');
      expect(component.meetings).toEqual(mockMeetings2);
    });

  });
});
