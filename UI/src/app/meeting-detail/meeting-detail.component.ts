import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { Meeting } from '../meeting/meeting';
import { MeetingService } from '../meeting/meeting.service';
import { UserClaimsService } from '../shared/user-claims.service';
import { UUID } from 'angular2-uuid';

@Component({
	selector: 'app-meeting-detail',
	templateUrl: './meeting-detail.component.html',
	styleUrls: ['./meeting-detail.component.css']
})
export class MeetingDetailComponent implements OnInit {
	meeting: Meeting = new Meeting('', '', '', '', '', '', []);
	showLoading: Boolean = true;
	votedForLoggedUser: Boolean = false;
	params = {};
	constructor(private meetingService: MeetingService, private activateRoute: ActivatedRoute,
		private toastrService: ToastrService, private router: Router, private userClaimsService: UserClaimsService) { }

	ngOnInit() {
		this.getMeeting();
	}

	getMeeting() {
		this.activateRoute.params.switchMap((params: Params) => this.meetingService.getMeetingById(params['id'])).subscribe(
			response => {
				this.meeting = response;
				this.getMeetingVotes();
			},
			error => {
				this.toastrService.error('Error while loading the meeting');
				this.showLoading = false;
				this.router.navigate(['meeting-list']);
			}
		);
	}

	getMeetingVotes() {
		this.activateRoute.params.switchMap((params: Params) => this.meetingService.getMeetingVotes(params['id'])).subscribe(
			response => {
				this.meeting.votes = response;
				this.showLoading = false;
			},
			error => {
				this.toastrService.error('Error while loading the meeting votes');
				this.showLoading = false;
				this.meeting.votes = null;
			}
		);
	}
	memberHasVotedMeeting() {
		if (this.meeting.votes == null) {
			return false;
		}
		return this.meeting.votes.indexOf(this.userClaimsService.username) > -1;
	}
	voteMeeting() {
		this.meetingService.voteMeeting(this.meeting.id, this.userClaimsService.username).subscribe(
			response => {
				this.toastrService.success('Operation successful!');
				this.meeting.votes.push(this.userClaimsService.username); /*not necessary*/
			},
			error => this.toastrService.error('Error when voting a meeting', error)
		);
	}

	cancelVoteMeeting() {
		this.meetingService.cancelVoteMeeting(this.meeting.id, this.userClaimsService.username).subscribe(
			response => {
				this.toastrService.success('Operation successful!');
				/* not neccesary*/
				const index: number = this.meeting.votes.indexOf(this.userClaimsService.username);
				if (index !== -1) {
					this.meeting.votes.splice(index, 1);
				}
			},
			error => this.toastrService.error('Error while cancel a meeting vote', error)
		);
	}
}
