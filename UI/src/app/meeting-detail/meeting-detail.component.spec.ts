import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { HttpClientModule } from '@angular/common/http';

import { MeetingDetailComponent } from './meeting-detail.component';
import { MeetingService } from '../meeting/meeting.service';
import { Meeting } from '../meeting/meeting';
import { UserClaimsService } from '../shared/user-claims.service';

describe('MeetingDetailComponent', () => {
  let component: MeetingDetailComponent;
  let meetingService: MeetingService;
  let toastrService: ToastrService;
  const router = { navigate: jasmine.createSpy('navigate') };
  const successSwitchMap = Observable.of({ 'id': 123 });
  const failSwitchMap = Observable.throw({ error: 'Error' });
  const activatedRoute = {
    params: {
      switchMap: jasmine.createSpy('').and.returnValues(successSwitchMap, failSwitchMap)
    }
  };
  const mockMeeting: Meeting = new Meeting(
    '1', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon',
    'Fede', 'AWS-API Gateway', '2017-05-18T10:20+01:00', '2017-07-20T18:37:49.459Z', ''
  );

  const mockMeetingVotes = ['user 2', 'name'];

  const userClaimsService = {
    username: 'name'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MeetingDetailComponent],
      providers: [MeetingService, { provide: ActivatedRoute, useValue: activatedRoute },
        { provide: Router, useValue: router }, { provide: UserClaimsService, useValue: userClaimsService }],
      imports: [FormsModule, HttpClientModule, RouterTestingModule, ToastrModule.forRoot()]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    const fixture = TestBed.createComponent(MeetingDetailComponent);
    component = fixture.componentInstance;
    meetingService = fixture.debugElement.injector.get(MeetingService);
    toastrService = fixture.debugElement.injector.get(ToastrService);
  });

  describe('when initializing', () => {
    it('should get meeting info', () => {
      spyOn(component, 'getMeeting');
      component.ngOnInit();
      expect(component.getMeeting).toHaveBeenCalled();
    });
  });

  describe('when loading meeting info', () => {
    it('should display meeting data', () => {
      expect(component.showLoading).toBe(true);
      spyOn(component, 'getMeetingVotes');
      spyOn(meetingService, 'getMeetingById').and.returnValue(Observable.of(mockMeeting));
      component.getMeeting();
      expect(component.getMeetingVotes).toHaveBeenCalled();
    });

    it('should display an error when service fails', () => {
      expect(component.showLoading).toBe(true);
      spyOn(meetingService, 'getMeetingById').and.returnValue(Observable.throw({ error: 'Error' }));
      spyOn(toastrService, 'error');
      component.getMeeting();
      expect(toastrService.error).toHaveBeenCalledWith('Error while loading the meeting');
      expect(component.showLoading).toBe(false);
      expect(component.meeting).toEqual(new Meeting('', '', '', '', '', '', []));
      expect(router.navigate).toHaveBeenCalledWith(['meeting-list']);
    });


  });

  // describe('when loading meeting votes data', () => {

  //   it('should display meeting votes data', () => {
  //     expect(component.showLoading).toBe(true);
  //     component.meeting = mockMeeting;
  //     spyOn(meetingService, 'getMeetingVotes').and.returnValue(Observable.of(mockMeetingVotes));
  //     component.getMeetingVotes();
  //     expect(component.showLoading).toBe(false);
  //     expect(component.meeting.votes).toEqual(mockMeetingVotes);
  //   });

  //   it('should display an error when service fails', () => {
  //     expect(component.showLoading).toBe(true);
  //     spyOn(meetingService, 'getMeetingVotes').and.returnValue(Observable.throw({ error: 'Error' }));
  //     spyOn(toastrService, 'error');
  //     component.getMeetingVotes();
  //     expect(toastrService.error).toHaveBeenCalledWith('Error while loading the meeting votes');
  //     expect(component.showLoading).toBe(false);
  //     expect(component.meeting.votes).toEqual(null);
  //     expect(router.navigate).toHaveBeenCalledWith(['meeting-list']);
  //   });

  //   it('should retrive true, member has voted meeting 1', () => {
  //     component.meeting = mockMeeting;
  //     spyOn(meetingService, 'getMeetingVotes').and.returnValue(Observable.of(mockMeetingVotes));
  //     // component.getMeetingVotes();
  //     expect(component.memberHasVotedMeeting()).toBe(true);
  //   });

  //   it('should retrieve false, member has not voted meeting 1', () => {
  //     mockMeetingVotes = ['4444', '5555', '2222'];
  //     component.meeting = mockMeeting;
  //     spyOn(meetingService, 'getMeetingVotes').and.returnValue(Observable.of(mockMeetingVotes));
  //     component.getMeetingVotes();
  //     expect(component.memberHasVotedMeeting()).toBe(false);
  //   });

  //   it('should retrieve false, service fails', () => {
  //     mockMeetingVotes = null;
  //     component.meeting = mockMeeting;
  //     spyOn(meetingService, 'getMeetingVotes').and.returnValue(Observable.throw({ error: 'Error' }));
  //     component.getMeetingVotes();
  //     expect(component.memberHasVotedMeeting()).toBe(false);
  //   });
  // });

  // describe('when vote a meeting', () => {
  //   it('should add a new vote for this meeting', () => {
  //     spyOn(meetingService, 'voteMeeting').and.returnValue(Observable.of([{}]));
  //     spyOn(toastrService, 'success');
  //     mockMeetingVotes = ['4444', '5555', '2222'];
  //     component.meeting = mockMeeting;
  //     spyOn(meetingService, 'getMeetingVotes').and.returnValue(Observable.of(mockMeetingVotes));
  //     component.getMeetingVotes();
  //     component.voteMeeting();

  //     expect(meetingService.voteMeeting).toHaveBeenCalled();
  //     expect(toastrService.success).toHaveBeenCalledWith('Operation successful!');
  //     expect(router.navigate).toHaveBeenCalledWith(['meeting-list']);
  //   });

  //   it('should display a toaster error when service fails', () => {
  //     spyOn(meetingService, 'voteMeeting').and.returnValue(Observable.throw({ error: 'Error' }));
  //     spyOn(toastrService, 'error');

  //     mockMeetingVotes = ['4444', '5555', '2222'];
  //     component.meeting = mockMeeting;
  //     component.getMeetingVotes();
  //     component.voteMeeting();

  //     expect(toastrService.error).toHaveBeenCalledWith('Error when voting a meeting', { error: 'Error' });
  //   });
  // });

  // describe('when cancel a vote meeting', () => {

  //   it('should display a toaster notification when vote was canceled', () => {
  //     mockMeetingVotes = ['name', '5555', '2222'];
  //     const totalVotes = mockMeetingVotes.length;
  //     component.meeting = mockMeeting;
  //     spyOn(meetingService, 'cancelVoteMeeting').and.returnValue(Observable.of([{}]));
  //     spyOn(toastrService, 'success');
  //     spyOn(meetingService, 'getMeetingVotes').and.returnValue(Observable.of(mockMeetingVotes));
  //     component.getMeetingVotes();
  //     component.cancelVoteMeeting();

  //     expect(component.meeting.votes.length).toBe(totalVotes - 1);
  //     expect(toastrService.success).toHaveBeenCalledWith('Operation successful!');
  //   });

  //   it('should display a toaster error when service fails', () => {
  //     spyOn(meetingService, 'cancelVoteMeeting').and.returnValue(Observable.throw({ error: 'Error' }));
  //     spyOn(toastrService, 'error');

  //     component.cancelVoteMeeting();

  //     expect(toastrService.error).toHaveBeenCalledWith('Error while cancel a meeting vote', { error: 'Error' });
  //   });
  // });
});
