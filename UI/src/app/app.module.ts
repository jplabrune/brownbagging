import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AvatarModule } from 'ngx-avatar';
import { NgxTypeaheadModule } from 'ngx-typeahead';

import { MeetingService } from './meeting/meeting.service';
import { AuthGuardService } from './shared/auth-guard.service';
import { UserClaimsService } from './shared/user-claims.service';

import { TruncatePipe } from './pipes/pipeTruncate.pipe';

import { AppComponent } from './app.component';
import { MeetingListComponent } from './meeting-list/meeting-list.component';
import { NewMeetingComponent } from './new-meeting/new-meeting.component';
import { EditMeetingComponent } from './edit-meeting/edit-meeting.component';
import { MeetingDetailComponent } from './meeting-detail/meeting-detail.component';
import { SpeakerMeetingsComponent } from './speaker-meetings/speaker-meetings.component';
import { BannerComponent } from './banner/banner.component';
import { BannerService } from './banner/banner.service';
import { SideNavComponent } from './side-nav/side-nav.component';

@NgModule({
  declarations: [
    MeetingListComponent,
    NewMeetingComponent,
    EditMeetingComponent,
    AppComponent,
    MeetingDetailComponent,
    SpeakerMeetingsComponent,
    TruncatePipe,
    BannerComponent,
    SideNavComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    AvatarModule,
    NgxTypeaheadModule
  ],
  providers: [ MeetingService, AuthGuardService, UserClaimsService, BannerService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
