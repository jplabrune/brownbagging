import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { SideNavComponent } from './side-nav.component';

describe('SideNavComponent', () => {
  let component: SideNavComponent;
  let fixture: ComponentFixture<SideNavComponent>;
  const router = {
    url: 'home'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideNavComponent ],
      providers: [ { provide: Router, useValue: router } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should close side-nav when click any button', () => {
    spyOn(component.isClose, 'emit').and.returnValue(true);
    component.closeNav();
    expect(component.isClose.emit).toHaveBeenCalledWith(true);
  });

  it('should verify if string is contains in current url', () => {
    expect(component.urlIncludes('home')).toBe(true);
    expect(component.urlIncludes('page')).toBe(false);
  });
});
