import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent {
  @Output() isClose = new EventEmitter<boolean>();

  constructor(private router: Router) { }

  closeNav() {
    this.isClose.emit(true);
  }

  urlIncludes(myUrl: string): boolean {
    return this.router.url.includes(myUrl);
  }
}
