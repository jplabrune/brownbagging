import { async, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { NewMeetingComponent } from './new-meeting.component';
import { MeetingService } from '../meeting/meeting.service';
import { Meeting } from '../meeting/meeting';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxTypeaheadModule } from 'ngx-typeahead';

describe('NewMeeting Component', () => {
  let component: NewMeetingComponent;
  let newMeetingService: MeetingService;
  let toastrService: ToastrService;
  const router = { navigate: jasmine.createSpy('navigate') };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewMeetingComponent ],
      providers: [ MeetingService, { provide: Router, useValue: router } ],
      imports: [ FormsModule, HttpClientModule, ToastrModule.forRoot(), NgbModule.forRoot(), NgxTypeaheadModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    const fixture = TestBed.createComponent(NewMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    newMeetingService = fixture.debugElement.injector.get(MeetingService);
    toastrService = fixture.debugElement.injector.get(ToastrService);
  });

  it('should create component and set properties', () => {
    expect(component.model).toEqual({});
    expect(component.datePicker).toBeUndefined();
    expect(component.time).toEqual({ hour: 0, minute: 0 });
    expect(component.today).toBeDefined();
    expect(component.jsonDataUrl).toBe('assets/mock_files/people.json');
  });

  describe('when submitting the form', () => {
    it('should create a new meeting', () => {
      spyOn(newMeetingService, 'createMeeting').and.returnValue(Observable.of([{}]));
      spyOn(toastrService, 'success');

      component.model = {title: 'title', description: 'descripcion', speaker: 'name', initDate: '2017-07-20T18:37:49.459Z'};

      component.speakerSelected('name');
      expect(component.model.speaker).toBe('name');
      expect(component.typeAheadSpeaker).toBe('name');
      component.model.speaker = ' ';
      component.verifyUser();
      expect(component.model.speaker).toBe('');
      expect(component.typeAheadSpeaker).toBe('');

      component.onSubmit();

      expect(newMeetingService.createMeeting).toHaveBeenCalledWith(jasmine.any(Meeting));
      expect(toastrService.success).toHaveBeenCalledWith('Operation successful!');
      expect(router.navigate).toHaveBeenCalledWith(['meeting-list']);
    });

    it('should create a new meeting with undefined time if date is not selected ', () => {
      spyOn(newMeetingService, 'createMeeting').and.returnValue(Observable.of([{}]));
      spyOn(toastrService, 'success');
      this.datePicker = null;
      component.model = {title: 'title', description: 'descripcion', speaker: 'name'};
      const today = new Date();
      component.datePicker = { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() };

      component.onSubmit();

      const date = new Date(component.datePicker.year, component.datePicker.month - 1, component.datePicker.day,
        0, 0, 0, 0);
      expect(component.model.initDate).toEqual(date);
      expect(newMeetingService.createMeeting).toHaveBeenCalledWith(jasmine.any(Meeting));
      expect(toastrService.success).toHaveBeenCalledWith('Operation successful!');
      expect(router.navigate).toHaveBeenCalledWith(['meeting-list']);
    });

    it('should display a toaster error when service fails', () => {
      spyOn(newMeetingService, 'createMeeting').and.returnValue(Observable.throw({error: 'Error'}));
      spyOn(toastrService, 'error');

      component.model = {title: 'title', description: 'descripcion', speaker: 'name'};
      component.onSubmit();

      expect(toastrService.error).toHaveBeenCalledWith('Error when creating a meeting', {error: 'Error'});
    });
  });

  it('should set current date time when selecting today', () => {
    const today = new Date();

    component.selectToday();

    expect(component.datePicker.day).toBe(today.getDate());
    expect(component.datePicker.month).toBe(today.getMonth() + 1);
    expect(component.datePicker.year).toBe(today.getFullYear());
  });
});
