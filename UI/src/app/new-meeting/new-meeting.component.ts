import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService, ToastrConfig } from 'ngx-toastr';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { Meeting } from '../meeting/meeting';
import { MeetingService } from '../meeting/meeting.service';
import { environment } from '../../environments/environment';
import 'rxjs/add/observable/of';
import { UUID } from 'angular2-uuid';

@Component({
  selector: 'app-new-meeting',
  templateUrl: './new-meeting.component.html',
  styleUrls: ['./new-meeting.component.css'],
})
export class NewMeetingComponent {
  model: any = { };
  datePicker: NgbDateStruct;
  time: any = { hour: 0, minute: 0 };
  today: Date = new Date();
  jsonDataUrl: String = environment.servicesUrl.getPeople;
  typeAheadSpeaker: any = '';

  constructor(private meetingService: MeetingService, private router: Router, private toastrService: ToastrService) {}

  onSubmit() {
    const id = UUID.UUID();
    if (!this.datePicker) {
      this.model.initDate = undefined;
    } else {
      const date = new Date(this.datePicker.year, this.datePicker.month - 1, this.datePicker.day, this.time.hour, this.time.minute, 0, 0);
      this.model.initDate = date;
    }

    const objectToSend: Meeting = new Meeting(id, this.model.title, this.model.description, this.model.speaker,
      new Date(), this.model.initDate, []);
    this.meetingService.createMeeting(objectToSend).subscribe(
      newMeetingResponse => {
        this.toastrService.success('Operation successful!');
        this.router.navigate(['meeting-list']);
      },
      error => this.toastrService.error('Error when creating a meeting', error)
    );
  }

  selectToday() {
    const today = new Date();
    this.datePicker = {year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate()};
  }

  speakerSelected(speaker) {
    this.typeAheadSpeaker = speaker;
    this.model.speaker = speaker;
  }

  verifyUser() {
    if (this.typeAheadSpeaker !== this.model.speaker) {
      this.typeAheadSpeaker = '';
      this.model.speaker = '';
    }
  }
}
