export const environment = {
  production: true,
  useJson: false,
  envName: 'production',
  servicesUrl: {
    getMeetings: '',
    getMeetingById: '',
    deleteMeeting: '',
    createMeeting: '',
    updateMeeting: '',
    getAvatar: '',
    getMeetingVotes: '',
    voteMeeting: '',
    cancelVoteMeeting: '',
    getPeople: 'assets/mock_files/people.json'
  }
};
