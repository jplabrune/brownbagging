// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  useJson: true,
  envName: 'mock',
  servicesUrl: {
    getMeetings: 'assets/mock_files/mock-meetings.json',
    getMeetingById: 'assets/mock_files/mock-meeting_{0}.json',
    deleteMeeting: '/',
    createMeeting: '/',
    updateMeeting: '/',
    getAvatar: 'assets/avatar_images/<username>',
    getMeetingVotes: 'assets/mock_files/mock-meeting-votes_{0}.json',
    voteMeeting: '/',
    cancelVoteMeeting: '/',
    getPeople: 'assets/mock_files/people.json'
  }
};
