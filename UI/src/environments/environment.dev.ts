// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  useJson: false,
  envName: 'dev',
  servicesUrl: {
    getMeetings: 'http://localhost:35555/api/meetings',
    getMeetingById: 'http://localhost:35555/api/meeting/{id}',
    deleteMeeting: 'http://localhost:35555/api/meeting/{id}',
    createMeeting: 'http://localhost:35555/api/meeting',
    updateMeeting: 'http://localhost:35555/api/meeting/{id}',
    getAvatar: '',
    getMeetingVotes: '',
    voteMeeting: '',
    cancelVoteMeeting: '',
    getPeople: 'assets/mock_files/people.json'
  }
};
