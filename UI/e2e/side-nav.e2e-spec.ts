import { element, by } from 'protractor';
import { Navigation } from './navigation.po';

describe('Side-Nav', () => {
  const nav: Navigation = new Navigation();

  beforeAll(() => {
    nav.login('admin', 'admin');
  });

  describe('when user is in the home page', () => {
    it('should only view the new-meeting button in the menu', function(){
      element(by.id('buttonOpenNav')).click().then(function(){
        expect(element(by.id('buttonNew')).isDisplayed()).toBe(true);
        expect(element(by.id('buttonHome')).isDisplayed()).toBe(false);
      });
    });

    it('should redirect to new meeting page when user clicks on New Meeting', function(){
      element(by.id('buttonNew')).click().then(function(){
        expect(nav.urlIncludes('new-meeting')).toBe(true);
      });
    });
  });

  describe('when user is in the new-meeting page', () => {
    beforeAll(() => {
      nav.navigateTo('/new-meeting');
    });

    it('should only view the home button in the menu', function(){
      element(by.id('buttonOpenNav')).click().then(function(){
        expect(element(by.id('buttonHome')).isDisplayed()).toBe(true);
        expect(element(by.id('buttonNew')).isDisplayed()).toBe(false);
      });
    });

    it('should redirect to new meeting page when user clicks on New Meeting', function(){
      element(by.id('buttonHome')).click().then(function(){
        expect(nav.urlIncludes('meeting-list')).toBe(true);
      });
    });
  });
});
