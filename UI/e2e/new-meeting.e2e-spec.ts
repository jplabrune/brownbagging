import { NewMeetingPage } from './new-meeting.po';
import { Navigation } from './navigation.po';
import { browser, element, by, Key } from 'protractor';

describe('New meeting', () => {
  const page: NewMeetingPage = new NewMeetingPage();
	const nav: Navigation = new Navigation();

	beforeAll(() => {
		nav.login('admin', 'admin');
	});

  beforeEach(() => {
    nav.navigateTo('/new-meeting');
	});

  it('should display an error message when submitting an invalid form', () => {
		page.clickButtonById('buttonSubmit');

		expect(nav.urlIncludes('new-meeting')).toBe(true);
  });

	it('should complete form attributes and submit it', () => {
		element(by.id('title')).sendKeys('Angular');
		element(by.id('description')).sendKeys('Descripcion de la meeting');

		element(by.id('speaker')).sendKeys('keys');
		element.all(by.css('.list-group-item')).get(2).click();
		expect(element(by.id('speaker')).getAttribute('ng-reflect-model')).toBe('FerDi');

		element(by.id('speaker')).clear().then(() => {
			element(by.id('speaker')).sendKeys('keys').then(() => {
				element(by.id('speaker')).sendKeys(Key.ESCAPE).then(() => {
					element(by.id('title')).click().then(() => {
						expect(element(by.id('speaker')).getAttribute('ng-reflect-model')).toBe('');
					});
				});
			});
		});

		page.clickButtonById('buttonSubmit');

		expect(nav.urlIncludes('meeting-list')).toBe(true);
	});

	it('should cancel form', () => {
		page.clickButtonById('cancelButton');

		expect(nav.urlIncludes('meeting-list')).toBe(true);
	});
});
