import { browser, element, by, until, Key } from 'protractor';

export class MeetingListPage {
	waitConfirmDialog() {
		browser.wait(element(by.id('confirm-dialog')).isDisplayed, 2000);
	}

	clickButtonById(button) {
		element(by.id(button)).click();
	}

	getMeetingsCount() {
		const meetingList = element.all(by.css('.card'));
		return meetingList.count();
	}

	getFirstMeetingTitle() {
		return element.all(by.css('.card-title')).get(0).getText();
	}

	sortBy(key) {
		let sortId = '';
		switch (key) {
			case 'title':
				sortId = 'md-option-0';
				break;
			case 'speaker':
				sortId = 'md-option-1';
				break;
			case 'lastUpdate':
				sortId = 'md-option-2';
				break;
			case 'initDate':
				sortId = 'md-option-3';
				break;
			default:
				break;
		}

		element(by.id('sortby')).click().then(function() {
			element(by.id(sortId)).click();
		});
	}

	filterBySearch(query) {
		const cards = element.all(by.css('.card-link'));
		element(by.id('filterSearchField')).clear().then(function() {
			if (query !== '') {
				element(by.id('filterSearchField')).sendKeys(query);
			} else {
				element(by.id('filterSearchField')).sendKeys('a');
				element(by.id('filterSearchField')).sendKeys(Key.BACK_SPACE);
			}
			element(by.id('buttonSearch')).click();
		});
	}

	countPresentElement(cssId: string) {
		let cont = 0;
		return element.all(by.css(cssId)).each(item => {
			item.isDisplayed().then(function(bool) {
				if (bool) {
					cont++;
				}
			});
		}).then(() => {
			return cont;
		});
	}
}
