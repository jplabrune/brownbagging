import { element, by } from 'protractor';

import { Navigation } from './navigation.po';

describe('Speaker meetings', () => {
  const nav: Navigation = new Navigation();

  beforeAll(() => {
    nav.login('admin', 'admin');
    nav.navigateTo('/speaker-meetings/FerDi');
  });

  it('should display title, description, meeting date and last update of each meeting', () => {
    expect(element(by.id('speaker')).getText()).toBe('Meetings of FerDi');
    expect(element(by.id('meetingsTable')).isPresent()).toBeTruthy();
    expect(element(by.id('title_0')).getText()).toBe('AWS-DynamoDB');
    expect(element(by.id('description_0')).getText()).toBe('Una brownbag sobre como interactuar con dynamoDB en la conso...');
    expect(element(by.id('initDate_0')).getText()).toBe('Jul 20, 2015, 20:00');
    expect(element(by.id('lastUpdate_0')).getText()).toBe('Jan 22, 2017, 18:37');
  });
});
