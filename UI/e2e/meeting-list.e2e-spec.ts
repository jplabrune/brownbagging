import { MeetingListPage } from './meeting-list.po';
import { element, by } from 'protractor';
import { Navigation } from './navigation.po';

describe('Meeting list', () => {
  const page: MeetingListPage = new MeetingListPage();
  const nav: Navigation = new Navigation();

  describe('when any user is logged', () => {
    beforeAll(() => {
      nav.login('admin', 'admin');
      nav.navigateTo('/meeting-list');
    });

    it('should display the meetings list', () => {
      expect(page.getMeetingsCount()).toBe(15);
    });

    describe('when sorting the meeting list', function(){
      it('should sort by last update date when last update date is selected', () => {
        page.sortBy('lastUpdate');
        expect(page.getFirstMeetingTitle()).toBe('Elasticsearch');
      });

      it('should sort by title when title is selected', () => {
        page.sortBy('title');
        expect(page.getFirstMeetingTitle()).toBe('AWS-API Gateway');
      });

      it('should invert list order when invert is clicked', () => {
        page.clickButtonById('inverse');
        expect(page.getFirstMeetingTitle()).toBe('Protractor');
      });

      it('should sort by speaker when speaker name is selected', () => {
        page.sortBy('speaker');
        expect(page.getFirstMeetingTitle()).toBe('Elasticsearch');
      });

      it('should sort by init date when init date is selected', () => {
        page.sortBy('initDate');
        expect(page.getFirstMeetingTitle()).toBe('AWS-API Gateway');
      });
    });

    describe('when filtering by search', function() {
      it('should display only the meetings that include the query string', () => {
        page.filterBySearch('S3');
        expect(page.getMeetingsCount()).toBe(1);

        page.filterBySearch('fede');
        expect(page.getMeetingsCount()).toBe(2);

        page.filterBySearch('empty');
        expect(page.getMeetingsCount()).toBe(0);

        page.filterBySearch('');
        expect(page.getMeetingsCount()).toBe(15);
      });
    });
  });

  describe('when an admin is logged in', function() {
    beforeAll(() => {
      nav.login('admin', 'admin');
      nav.navigateTo('/meeting-list');
    });

    it('should can delete any meeting', () => {
      expect(element.all(by.css('.fa-trash')).count()).toBe(15);
    });

    it('should can edit any meeting', () => {
      expect(element.all(by.css('.fa-pencil-square-o')).count()).toBe(15);
    });

    describe('when deleting a meeting', function(){
      it('should display confirm dialog and not remove the meeting if user cancels actions', () => {
        page.clickButtonById('deleteMeeting_1');
        page.waitConfirmDialog();
        expect(element(by.id('confirm-dialog')).isDisplayed()).toBe(true);

        page.clickButtonById('cancelButton');
        expect(element(by.id('confirm-dialog')).isPresent()).toBe(false);
        expect(page.getMeetingsCount()).toBe(15);
      });

      it('should display confirm dialog and remove the meeting if user confirm the action', () => {
        page.clickButtonById('deleteMeeting_1');
        page.waitConfirmDialog();
        expect(element(by.id('confirm-dialog')).isDisplayed()).toBe(true);

        page.clickButtonById('deleteButton');
      });
    });

    describe('when select meeting for update', function(){
      it('should route to edit-meeting view', () => {
        page.clickButtonById('editMeeting_1');
        expect(nav.urlIncludes('edit-meeting')).toBe(true);
      });
    });

    describe('when click on meeting title', function(){
      it('should route to meeting-detail view', () => {
        nav.navigateTo('/meeting-list');
        page.clickButtonById('title_1');
        expect(nav.urlIncludes('meeting-detail')).toBe(true);
      });
    });
  });
});
