import { element, by } from 'protractor';

export class MeetingDetail {
	elementExists(id: string) {
		expect(element(by.id(id)).isPresent()).toBe(true);
	}

	clickButtonById(button) {
		element(by.id(button)).click();
	}

	compareTextById(id: string, text: string) {
		return element(by.id(id)).getText().then(function (text2) {
			return text === text2;
		});
	}
}
