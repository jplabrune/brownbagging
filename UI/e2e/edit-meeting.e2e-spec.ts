import { EditMeeting } from './edit-meeting.po';
import { Navigation } from './navigation.po';
import { element, by , Key } from 'protractor';

describe('Edit Meeting', () => {
  const page: EditMeeting = new EditMeeting();
  const nav: Navigation = new Navigation();

  beforeAll(() => {
    nav.login('admin', 'admin');
  });

  beforeEach(() => {
    nav.navigateTo('/edit-meeting/1');
  });

  describe('when choosing a meeting', () => {
    it('should display meeting information', () => {
      expect(page.compareTextById('title', 'AWS-API Gateway')).toBe(true);
      expect(page.compareTextById('description', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon')).toBe(true);
      expect(element(by.id('speaker')).getAttribute('ng-reflect-model')).toBe('Juampa');
    });
  });

  describe('when updating a meeting', () => {
    it('should submit same information if no updates done and return to meetings list', () => {
      element(by.id('buttonUpdate')).click();

      expect(nav.urlIncludes('meeting-list')).toBeTruthy();
    });

    it('should submit new form attributes, update the meeting and return to meetings list', () => {
      element(by.id('title')).clear();
      element(by.id('description')).clear();
      element(by.id('speaker')).clear();

      element(by.id('title')).sendKeys('Angular');
      element(by.id('description')).sendKeys('Descripcion de la meeting');
      element(by.id('speaker')).sendKeys('keys');
      element.all(by.css('.list-group-item')).get(2).click();
      expect(element(by.id('speaker')).getAttribute('ng-reflect-model')).toBe('FerDi');

      element(by.id('speaker')).clear().then(() => {
        element(by.id('speaker')).sendKeys('keys').then(() => {
          element(by.id('speaker')).sendKeys(Key.ESCAPE).then(() => {
            element(by.id('title')).click().then(() => {
              expect(element(by.id('speaker')).getAttribute('ng-reflect-model')).toBe('');
            });
          });
        });
      });

      element(by.id('buttonUpdate')).click();

      expect(nav.urlIncludes('meeting-list')).toBeTruthy();
    });
  });

  describe('when the form is not completed', () => {
    it('should display a error message when submit an invalid form', () => {
      element(by.id('title')).clear().then(function(){
        element(by.id('title')).sendKeys('a');
        element(by.id('title')).sendKeys(Key.BACK_SPACE);
        element(by.id('buttonUpdate')).click();
      });

      expect(nav.urlIncludes('edit-meeting')).toBe(true);
    });
  });

  describe('when the update is cancelled', () => {
    it('should return to meetings list', () => {
      page.clickButtonById('cancelButton');

      expect(nav.urlIncludes('meeting-list')).toBeTruthy();
    });
  });
});
