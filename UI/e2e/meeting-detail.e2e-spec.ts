import { MeetingDetail } from './meeting-detail.po';
import { Navigation } from './navigation.po';

describe('Meeting detail', () => {
  const page: MeetingDetail = new MeetingDetail();
  const nav: Navigation = new Navigation();

  beforeAll(() => {
    nav.login('admin', 'admin');
    nav.navigateTo('/meeting-detail/1');
  });

  it('should display a correct meeting information', () => {
    expect(page.compareTextById('title', 'AWS-API Gateway')).toBe(true);
    expect(page.compareTextById('description', 'Una brownbag sobre como interactuar con dynamoDB en la consola de Amazon')).toBe(true);
    expect(page.compareTextById('speaker', 'Juampa')).toBe(true);
    expect(page.compareTextById('initDate', 'Meeting date: 7/20/2017, 3:37 PM')).toBe(true);
    expect(page.compareTextById('lastUpdate', 'Last Update: 5/18/2017, 6:20 AM')).toBe(true);
    expect(page.elementExists('voteMeetingBtn'));
    expect(page.compareTextById('voteMeetingCount', '2')).toBe(true);
  });


  describe('Logged user didn\'t vote meeting 1 , when click in vote button - ', () => {
    it('should show the vote meeting button', () => {
      expect(page.elementExists('voteMeetingBtn'));
    });

    it('should increase 1 vote to the vote meeting count', () => {
      expect(page.compareTextById('voteMeetingCount', '2')).toBe(true);
      page.clickButtonById('voteMeetingBtn');
      expect(page.compareTextById('voteMeetingCount', '3')).toBe(true);
    });

  });

  describe('when click in speaker name', () => {
    it('should navigate to speaker-meetings view', () => {
      page.clickButtonById('speaker');
      expect(nav.urlIncludes('speaker-meetings')).toBe(true);
    });
  });

  xdescribe('Logged user has voted meeting 2 , when click in vote button - ', () => {
    beforeAll(() => {
      nav.navigateTo('/meeting-detail/2');
    });
    it('should show the cancel vote meeting button', () => {
      expect(page.elementExists('cancelVoteMeetingBtn'));
    });

    it('should decrease 1 vote on the vote meeting count', () => {
      expect(page.compareTextById('voteMeetingCount', '2'));
      page.clickButtonById('cancelVoteMeetingBtn');
      expect(page.compareTextById('voteMeetingCount', '1')).toBe(true);
    });
  });
});
