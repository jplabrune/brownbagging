import { element, by } from 'protractor';
import { Navigation } from './navigation.po';

describe('Banner', () => {
  const nav: Navigation = new Navigation();

  beforeAll(() => {
    nav.login('admin', 'admin');
  });

  it('should redirect to the home when user clicks on app title', function(){
    element(by.id('appTitle')).click().then(function(){
      expect(nav.urlIncludes('meeting-list')).toBe(true);
    });
  });

  it('should show side-nav when user clicks on button for open it', function(){
    element(by.id('buttonOpenNav')).click().then(() => {
      expect(element(by.id('sideMenu')).isPresent()).toBe(true);
    });
  });

  it('should show image avatar', function(){
    expect(element(by.id('avatarImg')).isPresent()).toBe(false);
    expect(element(by.id('unknownImg')).isPresent()).toBe(true);
  });

  describe('when user not have avatar image', () => {
    it('should show unknown avatar', function(){
      nav.logout();
      nav.login('Juampa', '123456');
      expect(element(by.id('avatarImg')).isPresent()).toBe(false);
      expect(element(by.id('unknownImg')).isPresent()).toBe(true);
      nav.logout();
    });
  });
});
