import { element, by } from 'protractor';

export class EditMeeting {
	clickButtonById(button) {
		element(by.id(button)).click();
	}

	compareTextById(id: string, text: string) {
		return element(by.id(id)).getAttribute('value').then(function(text2) {
			return text === text2;
		});
	}
}
