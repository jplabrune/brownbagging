import { browser, element, by } from 'protractor';

export class NewMeetingPage {
	clickButtonById(button) {
		element(by.id(button)).click();
  }
}
