import { browser, element, by } from 'protractor';

export class Navigation {
	navigateTo(url: string) {
		browser.get(url);
	}

	urlIncludes(url: string) {
    return browser.getCurrentUrl().then(function(actualUrl) {
      return actualUrl.includes(url);
    });
	}

	login(username: string, password: string) {
    this.navigateTo('/');
    this.urlIncludes('login').then(isIncluded => {
      if (isIncluded) {
        element(by.id('username')).sendKeys(username);
        element(by.id('password')).sendKeys(password);
        element(by.buttonText('Login')).click();
        browser.ignoreSynchronization = true;
        browser.waitForAngular();
        element(by.buttonText('Yes, Allow')).isPresent().then(function(present){
          if (present) {
            element(by.buttonText('Yes, Allow')).click();
          }
        });
        browser.ignoreSynchronization = false;
        this.navigateTo('/');
      }
    });
  }

  logout() {
    browser.executeScript('$(\'#buttonLogOut\').click()');
	}
}
