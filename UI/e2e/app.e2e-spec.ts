import { Navigation } from './navigation.po';

describe('Initializing', () => {
  const nav: Navigation = new Navigation();

  beforeAll(() => {
    nav.login('admin', 'admin');
  });

  it('should login and redirect to application home page', function(){
    expect(nav.urlIncludes('meeting-list')).toBe(true);
  });
});
