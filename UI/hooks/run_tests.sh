#!/bin/sh
red="\033[0;31m"
yellow="\033[1;33m"
green="\033[1;32m"
reset="\033[0m"

printf "${yellow}Starting unit tests...${reset}\n"

ng test -e mock --single-run true

unitTestResults=$?
if [ $unitTestResults -eq 1 ]
then
  echo -e "${red}\nUnit tests FAILED${reset}"
  exit 1
else
  echo -e "${green}\nUnit tests are OK\n${reset}"
  printf "${yellow}Starting E2E tests...${reset}\n"
  ng e2e -e mock
  e2etestResults=$?
	if [ $e2etestResults -eq 1 ]
	then
    echo -e "${red}\nE2E tests FAILED${reset}"
    exit 1
	else
    echo -e "${green}\nE2E Tests are OK\n${reset}"    
	fi
fi
exit 0